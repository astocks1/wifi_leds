#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <WS2812FX.h>

#define LED_COUNT 70
#define LED_PIN 5
#define TIMER_MS 5000

// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)
WS2812FX ws2812fx = WS2812FX(LED_COUNT, LED_PIN, NEO_GRB    + NEO_KHZ800);

const char* ssid = "Stocks";
const char* password =  "andy1993";
const char* mqttServer = "192.168.1.7";
const int mqttPort = 1883;
//const char* mqttUser = "arduino";
//const char* mqttPassword = "arduino";
const char *mqtt_topic = "test";
const char *mqtt_topic2 = "color";

unsigned long now = 0;
unsigned long last_change = 0;

WiFiClient espClient;
PubSubClient client(espClient);


///////////////////////////
unsigned long color = 0xf6da09;
int  mod = 21;
int *p = &mod;
//////////////////////////


void setup() {

  Serial.begin(9600);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("Connecting to WiFi..");
  }
  Serial.println("Connected to the WiFi network2");

  client.setServer(mqttServer, mqttPort);
  client.setCallback(callback);

  while (!client.connected()) {
    Serial.println("Connecting to MQTT...");

    if (client.connect("ESP8266")) {

      Serial.println("connected");
      client.subscribe(mqtt_topic, 1);
      client.subscribe(mqtt_topic2, 1);


    } else {

      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);

    }
  }

  ws2812fx.init();
  ws2812fx.setBrightness(255);
  ws2812fx.setSpeed(1000);
  ws2812fx.setMode(FX_MODE_STATIC);
  ws2812fx.start();

}

void callback(char* topic, byte* payload, unsigned int length) {

  payload[length] = '\0';
  
  if (strcmp(topic,"test")==0){
  int pwmVal = atoi((char *)payload);
  ws2812fx.setMode(pwmVal);
  }

  if (strcmp(topic,"color")==0){
  int color = strtol((char *)payload, NULL, 16);
  Serial.println(color);
  ws2812fx.setColor(color);
  }
    
}

void loop() {
  client.loop();

  now = millis();

  ws2812fx.service();

  if (now - last_change > TIMER_MS) {
//    ws2812fx.setColor(color);
    last_change = now;
  }

  //   Serial.println(mod);


}
